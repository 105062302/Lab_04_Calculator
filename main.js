var ans = 0;
var exp = "z";

function numBtn(str)
{
    if(exp == "z")
        exp = str;
    else
        exp += str;
    document.getElementById("screen").value = exp;
}

function opBtn(str)
{
    switch(str)
    {
        case 'C':
            exp = 'z';
            break;
        case '(':
            if(exp == "z")
                exp = '(';
            else
                exp += '(';
            break;
        case ')':
            if(exp == "z")
                exp = '(';
            else
                exp += ')';
            break;
        case '/':
            if(exp == "z")
                exp = '/';
            else
                exp += '/';
            break;
        case '*':
            if(exp == "z")
                exp = '*';
            else
                exp += '*';
            break;
        case '-':
            if(exp == "z")
                exp = '-';
            else
                exp += '-';
            break;
        case '+':
            if(exp == "z")
                exp = '+';
            else
                exp += '+';
            break;
        case '=':
            try 
            {
                 eval(exp);
            }
            catch (e) 
            {
                document.getElementById("screen").value = "Error";
                exp = "z";
                return;
            }
            ans = eval(exp);
            document.getElementById("screen").value=ans;
            exp = "z";
            return;
        default:
            break;
    }
    if(exp == "z")
        document.getElementById("screen").value = 0;
    else 
        document.getElementById("screen").value = exp;
    return;
}